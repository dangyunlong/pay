
<?php

/*
 * ---------------------参数生成页-------------------------------
 * 用户 --> 点击购买 在您自己的服务器上生成新订单，并把计算好的订单信息传给您的前端网页。
 * 注意：
 * 1.key一定要在服务端计算，不要在网页中计算。
 * 2.token只能存放在服务端，不可以以任何形式存放在网页代码中（可逆加密也不行），也不可以通过url参数方式传入网页。
 * 3.接口跑通后，如果发现收款二维码是我们官方的，请检查APP是否正在运行。为保障您收款功能正常，如果您的收款手机APP掉线超过一分钟，就会触发代收款机制，详情请看网站帮助。
 * --------------------------------------------------------------
 */

    // 从网页传入 price 
    $price = $_POST["price"];

    // 从网页传入 type [1: 微信, 2: 支付宝]
    $type = $_POST["type"];

    // 填写 api_user
    $api_user = "55c1c0cf";

    // 填写 api_key 
    $api_key = "9230a059-3071-4766-94e0-14df9571068f";

    // 您系统内部生成的订单号, 每创建一个订单, 此订单号需要+1
    $order_id = "123456789";

    // 您自定义的用户信息, 方便在后台对账, 排查订单是由哪个用户发起的, 强烈建议加上
    $order_info = "username";

    // 用户支付成功之后, 跳转到的页面
    $redirect = 'http://www.example.com/pay_redirect.html';

    // 签名 
    $signature = md5($api_key. $api_user. $order_id. $order_info. $price. $redirect. $type);

    $ret['api_user'] = $api_user;
    $ret['price'] = $price;
    $ret['type'] = $type;
    $ret['redirect'] = $redirect;
    $ret['order_id'] = $order_id;
    $ret['order_info'] =$order_info;
    $ret['signature'] = $signature;

    echo jsonSuccess("OK", $ret, "");

    // 返回正确
    function jsonSuccess($msg= '', $data = '') {
        $return['msg']  = $msg;
        $return['data'] = $data;
        $return['code'] = 1;
        return json_encode($return);
    }
?>
